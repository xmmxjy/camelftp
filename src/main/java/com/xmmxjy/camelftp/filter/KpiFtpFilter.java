package com.xmmxjy.camelftp.filter;

import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileFilter;
import org.springframework.stereotype.Component;

/**
 * 文件过滤器类,过滤需要下载的文件
 */
@Component
public class KpiFtpFilter implements GenericFileFilter {
    @Override
    public boolean accept(GenericFile genericFile) {
        return true;
    }
}
