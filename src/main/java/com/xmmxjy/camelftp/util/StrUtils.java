package com.xmmxjy.camelftp.util;

import com.xiaoleilu.hutool.util.StrUtil;

public class StrUtils {

    public static String firstLower(String str) {
        if (StrUtil.isEmpty(str)) {
            return null;
        }
        return str.substring(0,1).toLowerCase() + str.substring(1);
    }
}
