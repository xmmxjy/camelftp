package com.xmmxjy.camelftp.router;

import com.xiaoleilu.hutool.util.StrUtil;
import com.xmmxjy.camelftp.config.KpiFtpConfig;
import com.xmmxjy.camelftp.filter.KpiFtpFilter;
import com.xmmxjy.camelftp.processor.KpiProcessor;
import com.xmmxjy.camelftp.util.StrUtils;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文件下载路由
 */

@Component
public class KpiRouter extends RouteBuilder {
    @Autowired
    private KpiFtpConfig kpiFtpConfig;
    @Autowired
    private KpiProcessor kpiProcessor;

    @Override
    public void configure() throws Exception {
        String ftpUlr = "ftp://" + kpiFtpConfig.getUrl() + ":" + kpiFtpConfig.getPort()
                + kpiFtpConfig.getDir() + "?username=" + kpiFtpConfig.getUsername() +
                "&password=" + kpiFtpConfig.getPassword() + "&delay=" + kpiFtpConfig.getDelay()
                + "&filter=#" + StrUtils.firstLower(KpiFtpFilter.class.getSimpleName());
                from(ftpUlr).to("file:" + kpiFtpConfig.getLocal()).process(kpiProcessor);
    }

}
