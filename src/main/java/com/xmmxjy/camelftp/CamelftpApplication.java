package com.xmmxjy.camelftp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelftpApplication {

    public static void main(String[] args) {
        SpringApplication.run(CamelftpApplication.class, args);
    }

}

