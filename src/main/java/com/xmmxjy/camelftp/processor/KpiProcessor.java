package com.xmmxjy.camelftp.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

/**
 * 文件下载完成以后后续的处理
 */
@Component
public class KpiProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {

    }
}
